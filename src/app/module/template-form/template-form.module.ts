import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TemplateFormRoutingModule } from './template-form-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    TemplateFormRoutingModule
  ]
})
export class TemplateFormModule { }
