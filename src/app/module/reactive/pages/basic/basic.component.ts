import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';

@Component({
  selector: 'app-basic',
  templateUrl: './basic.component.html',
  styleUrls: ['./basic.component.scss'],
})
export class BasicComponent implements OnInit {
  // miFormulario:FormGroup = new FormGroup({
  //   producto: new FormControl("RTX 4080ti"),
  //   precio: new FormControl(1500),
  //   existencias: new FormControl(0),
  // });

  miFormulario: FormGroup = this.fb.group({
    producto: [, [Validators.required, Validators.minLength(3)]],
    precio: [, [Validators.required, Validators.min(0)]],
    existencias: [, [Validators.required, Validators.min(0)]],
  });

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    // no usamos el setValue para poner valores por defecto porque nos vota errores
    // cuando no colocamos algun valor a un campo
    // this.miFormulario.setValue({
    //   producto: 'RTX 4080ti',
    //   precio: 1600,
    //   existencias: 1,
    // });
    this.miFormulario.reset({
      producto: 'RTX 4080ti',
      precio: 1600,
      // existencias: 1,
    });
  }

  campoEsValido(campo: string) {
    //https://itnext.io/understanding-angular-reactive-forms-241f9ed42c56
    //https://angular.io/guide/reactive-forms
    //https://itecnote.com/tecnote/angular-formgroup-get-vs-formgroup-controls-in-reactive-form-angular/
    //https://www.tektutorialshub.com/angular/formgroup-in-angular/
    return (
      this.miFormulario.controls[campo]?.errors &&
      this.miFormulario.controls[campo]?.touched
    );
    // TODO: https://stackoverflow.com/questions/49642926/formgroup-get-vs-formgroup-controls-in-reactive-form-angular
    //TODO: esto es lo mismo que el return de arriba
    // return (
    //   this.miFormulario.get(campo)?.errors &&
    //   this.miFormulario.get(campo)?.touched
    // );
  }

  guardar() {
    if (this.miFormulario.invalid) {
      console.log('formulario invalido');
      //marca todos los campos como tocados
      this.miFormulario.markAllAsTouched();
      return;
    }
    console.log(this.miFormulario.value);
    // resetea el formulario con todos los campos vacios
    this.miFormulario.reset();
  }
}
