import { Component, OnInit } from '@angular/core';
import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';

@Component({
  selector: 'app-dynamic',
  templateUrl: './dynamic.component.html',
  styleUrls: ['./dynamic.component.scss'],
})
export class DynamicComponent implements OnInit {
  miFormulario: FormGroup = this.fb.group({
    nombre: [, [Validators.required]],
    favoritos: this.fb.array(
      [this.fb.control('Metal Gear'), ['World of warcraf']],
      Validators.required
    ),
  });

  // para acceder al FormArray de favoritos tenemos que crear un nuevo FormControl independiente

  nuevoFavorito: FormControl = this.fb.control('', Validators.required);

  get favoritosArr() {
    // return this.miFormulario.controls['favoritos'] as FormArray;
    return this.miFormulario.get("favoritos") as FormArray;
  }

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {}

  campoEsValido(campo: string) {
    return (
      this.miFormulario.get(campo)?.errors &&
      this.miFormulario.get(campo)?.touched
    );
  }

  agregarFavorito() {
    if (this.nuevoFavorito.invalid) {
      return;
    }
    // cualquiera de las dos formas es valida
    const nuevoFav = this.fb.control(
      this.nuevoFavorito.value,
      Validators.required
    );
    // const nuevoFav = new FormControl(this.nuevoFavorito.value,Validators.required);
    this.favoritosArr.push(nuevoFav);
    this.nuevoFavorito.reset();
  }

  guardar() {
    if (this.miFormulario.invalid) {
      console.log('formulario invalido');
      //marca todos los campos como tocados
      this.miFormulario.markAllAsTouched();
      return;
    }
    console.log(this.miFormulario.value);
    // resetea el formulario con todos los campos vacios
    this.miFormulario.reset();
    //borra todos los elementos del arreglo
    this.favoritosArr.clear();
    // esto le colocamos por si el usuario da enter cuando tiene todos los campos llenos
    this.nuevoFavorito.reset();
  }

  borrarFavorito(indice: number) {
    this.favoritosArr.removeAt(indice);
  }
}

// https://medium.com/@bohndez.dev/custom-form-control-con-control-value-accessor-en-angular-5-6-o-7-f8f4030f105d
