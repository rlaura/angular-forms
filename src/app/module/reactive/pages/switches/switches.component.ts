import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-switches',
  templateUrl: './switches.component.html',
  styleUrls: ['./switches.component.scss'],
})
export class SwitchesComponent implements OnInit {
  miFormulario: FormGroup = this.fb.group({
    genero: ['M', Validators.required],
    notificaciones: [true, Validators.required],
    condiciones: [true, Validators.requiredTrue],
  });

  persona = {
    genero: 'F',
    notificaciones: true,
  };

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    this.miFormulario.reset({ ...this.persona, condiciones: false });
    //TODO: valueChanges emite un valor cada vez que se cambia algo en el formulario
    // this.miFormulario.valueChanges.subscribe((form) => {
    //   // delete form.condiciones;
    //   this.persona = form;
    // });
    //TODO: otra forma de hacerlo
    this.miFormulario.valueChanges.subscribe(({ condiciones, ...restoDeArgumentos }) => {
      this.persona = restoDeArgumentos;
    });

    //tambien se puede cer elcambio de un campo en especifico con cualquier campo del formulario
    // this.miFormulario.get('genero')?.valueChanges.subscribe((netValue) => {
    //   console.log(netValue);
    // });
  }

  guardar() {
    const formValue = { ...this.miFormulario.value };
    //para eliminar la propiedad del objeto
    delete formValue.condiciones;

    this.persona = formValue;

    console.log(formValue);
  }
}
