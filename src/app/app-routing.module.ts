import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'reactive',
    loadChildren: () =>
      import('./module/reactive/reactive.module').then((m) => m.ReactiveModule),
  },
  {
    path: 'template',
    loadChildren: () =>
    import('./module/template-form/template-form.module').then((m) => m.TemplateFormModule),
  },
  {
    path: 'auth',
    loadChildren: () =>
    import('./module/auth/auth.module').then((m) => m.AuthModule),
  },
  {
    path: '**',
    redirectTo: "reactive"
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
