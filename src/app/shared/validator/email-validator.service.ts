import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  AbstractControl,
  AsyncValidator,
  ValidationErrors,
} from '@angular/forms';
import { delay, map, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class EmailValidatorService implements AsyncValidator {
  constructor(private _http: HttpClient) {}

  validate(
    control: AbstractControl<any, any>
  ): Observable<ValidationErrors | null> {
    //el control practicamente es lo que se coloca en el campo
    const email = control.value;

    // console.log(email);

    //esta peticion http practicamente te da todos los usuarios con ese email
    return this._http
      .get<any[]>(`http://localhost:3000/usuarios?q=${email}`)
      .pipe(
        //usamos el delay para simular un retrazo para probar el estado del formulario en pending
        // delay(3000), 
        map((resp) => {
          //si lacantidad de usuarios que bota la peticion es 0 retornamos null
          return resp.length === 0 ? null : { emailTomado: true };
        })
      );
  }

  // registerOnValidatorChange?(fn: () => void): void {

  // }
}
